#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Equipe pedagógica'
SITENAME = 'Engenharia e Sociedade'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Sao_Paulo'
DEFAULT_DATE = 'fs'
DEFAULT_LANG = 'pt_br'
DEFAULT_DATE_FORMAT = '%d/%m/%Y'

THEME = "theme"
# HOME_COVER = "images/header.jpeg"
HOME_COVER = "images/ee001.png"
HEADER_COVERS_BY_CATEGORY = {}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
